BLACK = '#'
WHITE = ' '

def words_in_row(row: str) -> list[str]:
    words = row.split(BLACK)
    return [word for word in words if word]

def find_start_positions(grid: list[str]) -> list[int]:
    positions = []
    for i, row in enumerate(grid):
        words = words_in_row(row)
        pos = 0
        for word in words:
            start_index = row.find(word, pos)
            positions.append((i, start_index))
            pos = start_index + len(word)
        
    num_columns = len(grid[0])
    for j in range(num_columns):
        column = ''.join(row[j] for row in grid)
        words = words_in_row(column)
        pos = 0
        for word in words:
            start_index = column.find(word, pos)
            positions.append((start_index, j))
            pos = start_index + len(word)

    positions = list(set(positions))
    return positions

def number_positions(positions: list[tuple[int, int]]) -> list[tuple[int, int, int]]:
    numbered_positions = []
    number = 1
    for (i, j) in sorted(positions):
        numbered_positions.append((i, j, number))
        number += 1
    return numbered_positions
    

def mark_crossword(grid: list[str]): 
    start_positions = find_start_positions(grid)
    numbered_positions = number_positions(start_positions)
    return numbered_positions

grid = ["##   ##", "     # ", "##  ## ", " #   ##"]
numbered_positions = mark_crossword(grid)

print(numbered_positions)
