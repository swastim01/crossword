WHITE = " "
BLACK = "#"
SIZE = 15

number = 1

def number_crossword(input: list[str]):
    for row_index, row in enumerate(input):
        for column_index, ch in enumerate(row):
            if ch == WHITE:
                return row_index, column_index, number

print(number_crossword(["##   ##", "     # "]))

def rows_col(char: str, input: list[str]):
    for row_index, row in enumerate(input):
        for column_index, ch in enumerate(row):
            if ch == char:    
                return row_index, column_index

   
def words_in_row(row: str):  
    words = row.split(BLACK)
    words = [word for word in words if word]  
    return words

print(words_in_row("##   ##"))

def iterate_words_list(word: str, row: str, input: list[str]):
    compare = list(zip(list(word), list(row)))
    for i in compare:
        if i[0] == i[1] == WHITE:
            global number
            number += 1
            return rows_col(i[0], input)




print(iterate_words_list("   ","     # ",["##   ##", "     # "] ))
