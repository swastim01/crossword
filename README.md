# Crossword

This project aims to return the numbers on the crossword. These numbers correspond to where the words are supposed to be entered by the users playing the crossword.

The numbers are returned along with the row and column they pertain to.

Project authors:
Swasti Mishra
Urnisha Banerjee
Priyanshi Jain
Herman Kaur